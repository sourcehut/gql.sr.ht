module git.sr.ht/~sircmpwn/gql.sr.ht

go 1.14

require (
	git.sr.ht/~sircmpwn/getopt v0.0.0-20191230200459-23622cc906b3
	github.com/99designs/gqlgen v0.11.4-0.20200512031635-40570d1b4d70
	github.com/Masterminds/squirrel v1.4.0
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/fernet/fernet-go v0.0.0-20191111064656-eff2850e6001
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/lib/pq v1.5.2
	github.com/martinlindhe/base36 v1.0.0
	github.com/matryer/moq v0.0.0-20200310130814-7721994d1b54 // indirect
	github.com/mitchellh/mapstructure v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/urfave/cli/v2 v2.2.0 // indirect
	github.com/vaughan0/go-ini v0.0.0-20130923145212-a98ad7ee00ec
	github.com/vektah/dataloaden v0.3.0 // indirect
	github.com/vektah/gqlparser v1.3.1
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/tools v0.0.0-20200519015757-0d0afa43d58a // indirect
	gopkg.in/mail.v2 v2.3.1
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
