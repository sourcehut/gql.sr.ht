# gql.sr.ht

This is a support library which houses code common to each *.sr.ht GraphQL API
implementation.

**NOTICE**: this library is obsolete and due to be deleted. See
[core-go](https://git.sr.ht/~sircmpwn/core-go) instead.
